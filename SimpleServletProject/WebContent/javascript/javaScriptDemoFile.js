var myObj={
    "prop1" : "Hello",
    "prop2" : 123,
    "prop3" : true,
    "objProp" : {
        "innerProp" : "Inner property"
    }
};

var myArray = [100,"shoaib",3];
console.log(myArray[0]);
console.log(myArray[1]);
console.log(myArray[2]);
console.log(myArray.length);

console.log(myObj["prop1"]);
console.log(myObj["prop2"]);
console.log(myObj["prop3"]);
console.log(myObj.objProp.innerProp);


var a = function abc(name){
    console.log('Hello '+name);
    
}
//abc('shaoib');

//Annoynms function expression
var f = function foo(){
    console.log('hello function');
}

var executor = function(fn,name){
    fn(name);
}

executor(a,"Shoaib ali ano")

//...........................................
//Function in object
var myObj1 = {
    "testProp": true
}
